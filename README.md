# Go API client for vikunja

This is the documentation for the [Vikunja](http://vikunja.io) API. Vikunja is a cross-plattform Todo-application with a lot of features, such as sharing lists with users or teams. 

## Overview

This API client was generated by the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project.  By using the [swagger-spec](https://github.com/swagger-api/swagger-spec) from a remote server, you can easily generate an API client.

* API version: 0.8+21-854fde1e4c

## Authorization 

* **JWT-Auth:** Main authorization method, used for most of the requests. Needs `Authorization: Bearer <jwt-token>`-header to authenticate successfully. 
* **BasicAuth:** Only used when requesting tasks via caldav. 

#### HTTP basic authentication

```
auth := context.WithValue(context.Background(), sw.ContextBasicAuth, sw.BasicAuth{
	UserName: "username",
	Password: "password",
})
r, err := client.Service.Operation(auth, args)
```

#### JWTKeyAuth - API key 

```
auth := context.WithValue(context.Background(), sw.ContextAPIKey, sw.APIKey{
	Key: "APIKEY",
	Prefix: "Bearer", // Omit if not necessary.
})
r, err := client.Service.Operation(auth, args)
```

## Installation

Put the package under your project folder and add the following in import:

```
go get -u code.vikunja.io/go-sdk
```

## Documentation for API Endpoints

All URIs are relative to *https://localhost/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AssigneesApi* | [**TasksTaskIDAssigneesBulkPost**](docs/AssigneesApi.md#taskstaskidassigneesbulkpost) | **Post** /tasks/{taskID}/assignees/bulk | Add multiple new assignees to a task
*AssigneesApi* | [**TasksTaskIDAssigneesGet**](docs/AssigneesApi.md#taskstaskidassigneesget) | **Get** /tasks/{taskID}/assignees | Get all assignees for a task
*AssigneesApi* | [**TasksTaskIDAssigneesPut**](docs/AssigneesApi.md#taskstaskidassigneesput) | **Put** /tasks/{taskID}/assignees | Add a new assignee to a task
*AssigneesApi* | [**TasksTaskIDAssigneesUserIDDelete**](docs/AssigneesApi.md#taskstaskidassigneesuseriddelete) | **Delete** /tasks/{taskID}/assignees/{userID} | Delete an assignee
*LabelsApi* | [**LabelsGet**](docs/LabelsApi.md#labelsget) | **Get** /labels | Get all labels a user has access to
*LabelsApi* | [**LabelsIdDelete**](docs/LabelsApi.md#labelsiddelete) | **Delete** /labels/{id} | Delete a label
*LabelsApi* | [**LabelsIdGet**](docs/LabelsApi.md#labelsidget) | **Get** /labels/{id} | Gets one label
*LabelsApi* | [**LabelsIdPut**](docs/LabelsApi.md#labelsidput) | **Put** /labels/{id} | Update a label
*LabelsApi* | [**LabelsPut**](docs/LabelsApi.md#labelsput) | **Put** /labels | Create a label
*LabelsApi* | [**TasksTaskIDLabelsBulkPost**](docs/LabelsApi.md#taskstaskidlabelsbulkpost) | **Post** /tasks/{taskID}/labels/bulk | Update all labels on a task.
*LabelsApi* | [**TasksTaskLabelsGet**](docs/LabelsApi.md#taskstasklabelsget) | **Get** /tasks/{task}/labels | Get all labels on a task
*LabelsApi* | [**TasksTaskLabelsLabelDelete**](docs/LabelsApi.md#taskstasklabelslabeldelete) | **Delete** /tasks/{task}/labels/{label} | Remove a label from a task
*LabelsApi* | [**TasksTaskLabelsPut**](docs/LabelsApi.md#taskstasklabelsput) | **Put** /tasks/{task}/labels | Add a label to a task
*ListApi* | [**ListsGet**](docs/ListApi.md#listsget) | **Get** /lists | Get all lists a user has access to
*ListApi* | [**ListsIdDelete**](docs/ListApi.md#listsiddelete) | **Delete** /lists/{id} | Deletes a list
*ListApi* | [**ListsIdGet**](docs/ListApi.md#listsidget) | **Get** /lists/{id} | Gets one list
*ListApi* | [**ListsIdListusersGet**](docs/ListApi.md#listsidlistusersget) | **Get** /lists/{id}/listusers | Get users
*ListApi* | [**ListsIdPost**](docs/ListApi.md#listsidpost) | **Post** /lists/{id} | Updates a list
*ListApi* | [**NamespacesNamespaceIDListsPut**](docs/ListApi.md#namespacesnamespaceidlistsput) | **Put** /namespaces/{namespaceID}/lists | Creates a new list
*NamespaceApi* | [**NamespaceIdPost**](docs/NamespaceApi.md#namespaceidpost) | **Post** /namespace/{id} | Updates a namespace
*NamespaceApi* | [**NamespacesGet**](docs/NamespaceApi.md#namespacesget) | **Get** /namespaces | Get all namespaces a user has access to
*NamespaceApi* | [**NamespacesIdDelete**](docs/NamespaceApi.md#namespacesiddelete) | **Delete** /namespaces/{id} | Deletes a namespace
*NamespaceApi* | [**NamespacesIdGet**](docs/NamespaceApi.md#namespacesidget) | **Get** /namespaces/{id} | Gets one namespace
*NamespaceApi* | [**NamespacesIdListsGet**](docs/NamespaceApi.md#namespacesidlistsget) | **Get** /namespaces/{id}/lists | Get all lists in a namespace
*NamespaceApi* | [**NamespacesPut**](docs/NamespaceApi.md#namespacesput) | **Put** /namespaces | Creates a new namespace
*ServiceApi* | [**InfoGet**](docs/ServiceApi.md#infoget) | **Get** /info | Info
*SharingApi* | [**ListsIdTeamsGet**](docs/SharingApi.md#listsidteamsget) | **Get** /lists/{id}/teams | Get teams on a list
*SharingApi* | [**ListsIdTeamsPut**](docs/SharingApi.md#listsidteamsput) | **Put** /lists/{id}/teams | Add a team to a list
*SharingApi* | [**ListsIdUsersGet**](docs/SharingApi.md#listsidusersget) | **Get** /lists/{id}/users | Get users on a list
*SharingApi* | [**ListsIdUsersPut**](docs/SharingApi.md#listsidusersput) | **Put** /lists/{id}/users | Add a user to a list
*SharingApi* | [**ListsListIDTeamsTeamIDDelete**](docs/SharingApi.md#listslistidteamsteamiddelete) | **Delete** /lists/{listID}/teams/{teamID} | Delete a team from a list
*SharingApi* | [**ListsListIDTeamsTeamIDPost**](docs/SharingApi.md#listslistidteamsteamidpost) | **Post** /lists/{listID}/teams/{teamID} | Update a team &lt;-&gt; list relation
*SharingApi* | [**ListsListIDUsersUserIDDelete**](docs/SharingApi.md#listslistidusersuseriddelete) | **Delete** /lists/{listID}/users/{userID} | Delete a user from a list
*SharingApi* | [**ListsListIDUsersUserIDPost**](docs/SharingApi.md#listslistidusersuseridpost) | **Post** /lists/{listID}/users/{userID} | Update a user &lt;-&gt; list relation
*SharingApi* | [**ListsListSharesGet**](docs/SharingApi.md#listslistsharesget) | **Get** /lists/{list}/shares | Get all link shares for a list
*SharingApi* | [**ListsListSharesPut**](docs/SharingApi.md#listslistsharesput) | **Put** /lists/{list}/shares | Share a list via link
*SharingApi* | [**ListsListSharesShareDelete**](docs/SharingApi.md#listslistsharessharedelete) | **Delete** /lists/{list}/shares/{share} | Remove a link share
*SharingApi* | [**ListsListSharesShareGet**](docs/SharingApi.md#listslistsharesshareget) | **Get** /lists/{list}/shares/{share} | Get one link shares for a list
*SharingApi* | [**NamespacesIdTeamsGet**](docs/SharingApi.md#namespacesidteamsget) | **Get** /namespaces/{id}/teams | Get teams on a namespace
*SharingApi* | [**NamespacesIdTeamsPut**](docs/SharingApi.md#namespacesidteamsput) | **Put** /namespaces/{id}/teams | Add a team to a namespace
*SharingApi* | [**NamespacesIdUsersGet**](docs/SharingApi.md#namespacesidusersget) | **Get** /namespaces/{id}/users | Get users on a namespace
*SharingApi* | [**NamespacesIdUsersPut**](docs/SharingApi.md#namespacesidusersput) | **Put** /namespaces/{id}/users | Add a user to a namespace
*SharingApi* | [**NamespacesNamespaceIDTeamsTeamIDDelete**](docs/SharingApi.md#namespacesnamespaceidteamsteamiddelete) | **Delete** /namespaces/{namespaceID}/teams/{teamID} | Delete a team from a namespace
*SharingApi* | [**NamespacesNamespaceIDTeamsTeamIDPost**](docs/SharingApi.md#namespacesnamespaceidteamsteamidpost) | **Post** /namespaces/{namespaceID}/teams/{teamID} | Update a team &lt;-&gt; namespace relation
*SharingApi* | [**NamespacesNamespaceIDUsersUserIDDelete**](docs/SharingApi.md#namespacesnamespaceidusersuseriddelete) | **Delete** /namespaces/{namespaceID}/users/{userID} | Delete a user from a namespace
*SharingApi* | [**NamespacesNamespaceIDUsersUserIDPost**](docs/SharingApi.md#namespacesnamespaceidusersuseridpost) | **Post** /namespaces/{namespaceID}/users/{userID} | Update a user &lt;-&gt; namespace relation
*SharingApi* | [**SharesShareAuthPost**](docs/SharingApi.md#sharesshareauthpost) | **Post** /shares/{share}/auth | Get an auth token for a share
*TaskApi* | [**ListsIdPut**](docs/TaskApi.md#listsidput) | **Put** /lists/{id} | Create a task
*TaskApi* | [**TasksAllGet**](docs/TaskApi.md#tasksallget) | **Get** /tasks/all | Get tasks
*TaskApi* | [**TasksBulkPost**](docs/TaskApi.md#tasksbulkpost) | **Post** /tasks/bulk | Update a bunch of tasks at once
*TaskApi* | [**TasksIdAttachmentsAttachmentIDDelete**](docs/TaskApi.md#tasksidattachmentsattachmentiddelete) | **Delete** /tasks/{id}/attachments/{attachmentID} | Delete an attachment
*TaskApi* | [**TasksIdAttachmentsAttachmentIDGet**](docs/TaskApi.md#tasksidattachmentsattachmentidget) | **Get** /tasks/{id}/attachments/{attachmentID} | Get one attachment.
*TaskApi* | [**TasksIdAttachmentsGet**](docs/TaskApi.md#tasksidattachmentsget) | **Get** /tasks/{id}/attachments | Get  all attachments for one task.
*TaskApi* | [**TasksIdAttachmentsPut**](docs/TaskApi.md#tasksidattachmentsput) | **Put** /tasks/{id}/attachments | Upload a task attachment
*TaskApi* | [**TasksIdDelete**](docs/TaskApi.md#tasksiddelete) | **Delete** /tasks/{id} | Delete a task
*TaskApi* | [**TasksIdPost**](docs/TaskApi.md#tasksidpost) | **Post** /tasks/{id} | Update a task
*TaskApi* | [**TasksTaskIDRelationsDelete**](docs/TaskApi.md#taskstaskidrelationsdelete) | **Delete** /tasks/{taskID}/relations | Remove a task relation
*TaskApi* | [**TasksTaskIDRelationsPut**](docs/TaskApi.md#taskstaskidrelationsput) | **Put** /tasks/{taskID}/relations | Create a new relation between two tasks
*TeamApi* | [**TeamsGet**](docs/TeamApi.md#teamsget) | **Get** /teams | Get teams
*TeamApi* | [**TeamsIdDelete**](docs/TeamApi.md#teamsiddelete) | **Delete** /teams/{id} | Deletes a team
*TeamApi* | [**TeamsIdMembersPut**](docs/TeamApi.md#teamsidmembersput) | **Put** /teams/{id}/members | Add a user to a team
*TeamApi* | [**TeamsIdMembersUserIDDelete**](docs/TeamApi.md#teamsidmembersuseriddelete) | **Delete** /teams/{id}/members/{userID} | Remove a user from a team
*TeamApi* | [**TeamsIdPost**](docs/TeamApi.md#teamsidpost) | **Post** /teams/{id} | Updates a team
*TeamApi* | [**TeamsPut**](docs/TeamApi.md#teamsput) | **Put** /teams | Creates a new team
*UserApi* | [**LoginPost**](docs/UserApi.md#loginpost) | **Post** /login | Login
*UserApi* | [**RegisterPost**](docs/UserApi.md#registerpost) | **Post** /register | Register
*UserApi* | [**UserConfirmPost**](docs/UserApi.md#userconfirmpost) | **Post** /user/confirm | Confirm the email of a new user
*UserApi* | [**UserGet**](docs/UserApi.md#userget) | **Get** /user | Get user information
*UserApi* | [**UserPasswordPost**](docs/UserApi.md#userpasswordpost) | **Post** /user/password | Change password
*UserApi* | [**UserPasswordResetPost**](docs/UserApi.md#userpasswordresetpost) | **Post** /user/password/reset | Resets a password
*UserApi* | [**UserPasswordTokenPost**](docs/UserApi.md#userpasswordtokenpost) | **Post** /user/password/token | Request password reset token
*UserApi* | [**UsersGet**](docs/UserApi.md#usersget) | **Get** /users | Get users


## Documentation For Models

 - [CodeVikunjaIoWebHttpError](docs/CodeVikunjaIoWebHttpError.md)
 - [FilesFile](docs/FilesFile.md)
 - [ModelsApiUserPassword](docs/ModelsApiUserPassword.md)
 - [ModelsBulkAssignees](docs/ModelsBulkAssignees.md)
 - [ModelsBulkTask](docs/ModelsBulkTask.md)
 - [ModelsEmailConfirm](docs/ModelsEmailConfirm.md)
 - [ModelsLabel](docs/ModelsLabel.md)
 - [ModelsLabelTask](docs/ModelsLabelTask.md)
 - [ModelsLabelTaskBulk](docs/ModelsLabelTaskBulk.md)
 - [ModelsLinkSharing](docs/ModelsLinkSharing.md)
 - [ModelsList](docs/ModelsList.md)
 - [ModelsListUser](docs/ModelsListUser.md)
 - [ModelsMessage](docs/ModelsMessage.md)
 - [ModelsNamespace](docs/ModelsNamespace.md)
 - [ModelsNamespaceUser](docs/ModelsNamespaceUser.md)
 - [ModelsNamespaceWithLists](docs/ModelsNamespaceWithLists.md)
 - [ModelsPasswordReset](docs/ModelsPasswordReset.md)
 - [ModelsPasswordTokenRequest](docs/ModelsPasswordTokenRequest.md)
 - [ModelsRelatedTaskMap](docs/ModelsRelatedTaskMap.md)
 - [ModelsTask](docs/ModelsTask.md)
 - [ModelsTaskAssginee](docs/ModelsTaskAssginee.md)
 - [ModelsTaskAttachment](docs/ModelsTaskAttachment.md)
 - [ModelsTaskRelation](docs/ModelsTaskRelation.md)
 - [ModelsTeam](docs/ModelsTeam.md)
 - [ModelsTeamList](docs/ModelsTeamList.md)
 - [ModelsTeamMember](docs/ModelsTeamMember.md)
 - [ModelsTeamNamespace](docs/ModelsTeamNamespace.md)
 - [ModelsTeamUser](docs/ModelsTeamUser.md)
 - [ModelsTeamWithRight](docs/ModelsTeamWithRight.md)
 - [ModelsUser](docs/ModelsUser.md)
 - [ModelsUserLogin](docs/ModelsUserLogin.md)
 - [ModelsUserWithRight](docs/ModelsUserWithRight.md)
 - [V1Token](docs/V1Token.md)
 - [V1UserPassword](docs/V1UserPassword.md)
 - [V1VikunjaInfos](docs/V1VikunjaInfos.md)


