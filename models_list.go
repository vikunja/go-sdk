/*
 * Vikunja API
 *
 * This is the documentation for the [Vikunja](http://vikunja.io) API. Vikunja is a cross-plattform Todo-application with a lot of features, such as sharing lists with users or teams. <!-- ReDoc-Inject: <security-definitions> --> # Authorization **JWT-Auth:** Main authorization method, used for most of the requests. Needs `Authorization: Bearer <jwt-token>`-header to authenticate successfully.  **BasicAuth:** Only used when requesting tasks via caldav. <!-- ReDoc-Inject: <security-definitions> -->
 *
 * API version: 0.8+21-854fde1e4c
 * Contact: hello@vikunja.io
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type ModelsList struct {

	// A unix timestamp when this list was created. You cannot change this value.
	Created int32 `json:"created,omitempty"`

	// The description of the list.
	Description string `json:"description,omitempty"`

	// The unique, numeric id of this list.
	Id int32 `json:"id,omitempty"`

	// The user who created this list.
	Owner *ModelsUser `json:"owner,omitempty"`

	// An array of tasks which belong to the list.
	Tasks []ModelsTask `json:"tasks,omitempty"`

	// The title of the list. You'll see this in the namespace overview.
	Title string `json:"title,omitempty"`

	// A unix timestamp when this list was last updated. You cannot change this value.
	Updated int32 `json:"updated,omitempty"`
}
