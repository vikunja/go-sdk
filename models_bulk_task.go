/*
 * Vikunja API
 *
 * This is the documentation for the [Vikunja](http://vikunja.io) API. Vikunja is a cross-plattform Todo-application with a lot of features, such as sharing lists with users or teams. <!-- ReDoc-Inject: <security-definitions> --> # Authorization **JWT-Auth:** Main authorization method, used for most of the requests. Needs `Authorization: Bearer <jwt-token>`-header to authenticate successfully.  **BasicAuth:** Only used when requesting tasks via caldav. <!-- ReDoc-Inject: <security-definitions> -->
 *
 * API version: 0.8+21-854fde1e4c
 * Contact: hello@vikunja.io
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type ModelsBulkTask struct {

	// An array of users who are assigned to this task
	Assignees []ModelsUser `json:"assignees,omitempty"`

	// All attachments this task has
	Attachments []ModelsTaskAttachment `json:"attachments,omitempty"`

	// A unix timestamp when this task was created. You cannot change this value.
	Created int32 `json:"created,omitempty"`

	// The user who initially created the task.
	CreatedBy *ModelsUser `json:"createdBy,omitempty"`

	// The task description.
	Description string `json:"description,omitempty"`

	// Whether a task is done or not.
	Done bool `json:"done,omitempty"`

	// The unix timestamp when a task was marked as done.
	DoneAt int32 `json:"doneAt,omitempty"`

	// A unix timestamp when the task is due.
	DueDate int32 `json:"dueDate,omitempty"`

	// When this task ends.
	EndDate int32 `json:"endDate,omitempty"`

	// The task color in hex
	HexColor string `json:"hexColor,omitempty"`

	// The unique, numeric id of this task.
	Id int32 `json:"id,omitempty"`

	// An array of labels which are associated with this task.
	Labels []ModelsLabel `json:"labels,omitempty"`

	// The list this task belongs to.
	ListID int32 `json:"listID,omitempty"`

	// Determines how far a task is left from being done
	PercentDone float32 `json:"percentDone,omitempty"`

	// The task priority. Can be anything you want, it is possible to sort by this later.
	Priority int32 `json:"priority,omitempty"`

	// All related tasks, grouped by their relation kind
	RelatedTasks *ModelsRelatedTaskMap `json:"related_tasks,omitempty"`

	// An array of unix timestamps when the user wants to be reminded of the task.
	ReminderDates []int32 `json:"reminderDates,omitempty"`

	// An amount in seconds this task repeats itself. If this is set, when marking the task as done, it will mark itself as \"undone\" and then increase all remindes and the due date by its amount.
	RepeatAfter int32 `json:"repeatAfter,omitempty"`

	// When this task starts.
	StartDate int32 `json:"startDate,omitempty"`

	// A list of task ids to update
	TaskIds []int32 `json:"task_ids,omitempty"`

	// The task text. This is what you'll see in the list.
	Text string `json:"text,omitempty"`

	// A unix timestamp when this task was last updated. You cannot change this value.
	Updated int32 `json:"updated,omitempty"`
}
