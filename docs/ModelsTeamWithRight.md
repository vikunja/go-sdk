# ModelsTeamWithRight

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this relation was created. You cannot change this value. | [optional] [default to null]
**CreatedBy** | [***ModelsUser**](models.User.md) | The user who created this team. | [optional] [default to null]
**Description** | **string** | The team&#39;s description. | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this team. | [optional] [default to null]
**Members** | [**[]ModelsTeamUser**](models.TeamUser.md) | An array of all members in this team. | [optional] [default to null]
**Name** | **string** | The name of this team. | [optional] [default to null]
**Right** | **int32** |  | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this relation was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


