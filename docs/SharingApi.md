# \SharingApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListsIdTeamsGet**](SharingApi.md#ListsIdTeamsGet) | **Get** /lists/{id}/teams | Get teams on a list
[**ListsIdTeamsPut**](SharingApi.md#ListsIdTeamsPut) | **Put** /lists/{id}/teams | Add a team to a list
[**ListsIdUsersGet**](SharingApi.md#ListsIdUsersGet) | **Get** /lists/{id}/users | Get users on a list
[**ListsIdUsersPut**](SharingApi.md#ListsIdUsersPut) | **Put** /lists/{id}/users | Add a user to a list
[**ListsListIDTeamsTeamIDDelete**](SharingApi.md#ListsListIDTeamsTeamIDDelete) | **Delete** /lists/{listID}/teams/{teamID} | Delete a team from a list
[**ListsListIDTeamsTeamIDPost**](SharingApi.md#ListsListIDTeamsTeamIDPost) | **Post** /lists/{listID}/teams/{teamID} | Update a team &lt;-&gt; list relation
[**ListsListIDUsersUserIDDelete**](SharingApi.md#ListsListIDUsersUserIDDelete) | **Delete** /lists/{listID}/users/{userID} | Delete a user from a list
[**ListsListIDUsersUserIDPost**](SharingApi.md#ListsListIDUsersUserIDPost) | **Post** /lists/{listID}/users/{userID} | Update a user &lt;-&gt; list relation
[**ListsListSharesGet**](SharingApi.md#ListsListSharesGet) | **Get** /lists/{list}/shares | Get all link shares for a list
[**ListsListSharesPut**](SharingApi.md#ListsListSharesPut) | **Put** /lists/{list}/shares | Share a list via link
[**ListsListSharesShareDelete**](SharingApi.md#ListsListSharesShareDelete) | **Delete** /lists/{list}/shares/{share} | Remove a link share
[**ListsListSharesShareGet**](SharingApi.md#ListsListSharesShareGet) | **Get** /lists/{list}/shares/{share} | Get one link shares for a list
[**NamespacesIdTeamsGet**](SharingApi.md#NamespacesIdTeamsGet) | **Get** /namespaces/{id}/teams | Get teams on a namespace
[**NamespacesIdTeamsPut**](SharingApi.md#NamespacesIdTeamsPut) | **Put** /namespaces/{id}/teams | Add a team to a namespace
[**NamespacesIdUsersGet**](SharingApi.md#NamespacesIdUsersGet) | **Get** /namespaces/{id}/users | Get users on a namespace
[**NamespacesIdUsersPut**](SharingApi.md#NamespacesIdUsersPut) | **Put** /namespaces/{id}/users | Add a user to a namespace
[**NamespacesNamespaceIDTeamsTeamIDDelete**](SharingApi.md#NamespacesNamespaceIDTeamsTeamIDDelete) | **Delete** /namespaces/{namespaceID}/teams/{teamID} | Delete a team from a namespace
[**NamespacesNamespaceIDTeamsTeamIDPost**](SharingApi.md#NamespacesNamespaceIDTeamsTeamIDPost) | **Post** /namespaces/{namespaceID}/teams/{teamID} | Update a team &lt;-&gt; namespace relation
[**NamespacesNamespaceIDUsersUserIDDelete**](SharingApi.md#NamespacesNamespaceIDUsersUserIDDelete) | **Delete** /namespaces/{namespaceID}/users/{userID} | Delete a user from a namespace
[**NamespacesNamespaceIDUsersUserIDPost**](SharingApi.md#NamespacesNamespaceIDUsersUserIDPost) | **Post** /namespaces/{namespaceID}/users/{userID} | Update a user &lt;-&gt; namespace relation
[**SharesShareAuthPost**](SharingApi.md#SharesShareAuthPost) | **Post** /shares/{share}/auth | Get an auth token for a share


# **ListsIdTeamsGet**
> []ModelsTeamWithRight ListsIdTeamsGet(ctx, id, optional)
Get teams on a list

Returns a list with all teams which have access on a given list.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| List ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search teams by its name. | 

### Return type

[**[]ModelsTeamWithRight**](models.TeamWithRight.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdTeamsPut**
> ModelsTeamList ListsIdTeamsPut(ctx, id, list)
Add a team to a list

Gives a team access to a list.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
  **list** | [**ModelsTeamList**](ModelsTeamList.md)| The team you want to add to the list. | 

### Return type

[**ModelsTeamList**](models.TeamList.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdUsersGet**
> []ModelsUserWithRight ListsIdUsersGet(ctx, id, optional)
Get users on a list

Returns a list with all users which have access on a given list.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| List ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search users by its name. | 

### Return type

[**[]ModelsUserWithRight**](models.UserWithRight.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdUsersPut**
> ModelsListUser ListsIdUsersPut(ctx, id, list)
Add a user to a list

Gives a user access to a list.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
  **list** | [**ModelsListUser**](ModelsListUser.md)| The user you want to add to the list. | 

### Return type

[**ModelsListUser**](models.ListUser.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListIDTeamsTeamIDDelete**
> ModelsMessage ListsListIDTeamsTeamIDDelete(ctx, listID, teamID)
Delete a team from a list

Delets a team from a list. The team won't have access to the list anymore.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **listID** | **int32**| List ID | 
  **teamID** | **int32**| Team ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListIDTeamsTeamIDPost**
> ModelsTeamList ListsListIDTeamsTeamIDPost(ctx, listID, teamID, list)
Update a team <-> list relation

Update a team <-> list relation. Mostly used to update the right that team has.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **listID** | **int32**| List ID | 
  **teamID** | **int32**| Team ID | 
  **list** | [**ModelsTeamList**](ModelsTeamList.md)| The team you want to update. | 

### Return type

[**ModelsTeamList**](models.TeamList.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListIDUsersUserIDDelete**
> ModelsMessage ListsListIDUsersUserIDDelete(ctx, listID, userID)
Delete a user from a list

Delets a user from a list. The user won't have access to the list anymore.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **listID** | **int32**| List ID | 
  **userID** | **int32**| User ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListIDUsersUserIDPost**
> ModelsListUser ListsListIDUsersUserIDPost(ctx, listID, userID, list)
Update a user <-> list relation

Update a user <-> list relation. Mostly used to update the right that user has.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **listID** | **int32**| List ID | 
  **userID** | **int32**| User ID | 
  **list** | [**ModelsListUser**](ModelsListUser.md)| The user you want to update. | 

### Return type

[**ModelsListUser**](models.ListUser.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListSharesGet**
> []ModelsLinkSharing ListsListSharesGet(ctx, list, optional)
Get all link shares for a list

Returns all link shares which exist for a given list

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **list** | **int32**| List ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **list** | **int32**| List ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search shares by hash. | 

### Return type

[**[]ModelsLinkSharing**](models.LinkSharing.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListSharesPut**
> ModelsLinkSharing ListsListSharesPut(ctx, list, label)
Share a list via link

Share a list via link. The user needs to have write-access to the list to be able do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **list** | **int32**| List ID | 
  **label** | [**ModelsLinkSharing**](ModelsLinkSharing.md)| The new link share object | 

### Return type

[**ModelsLinkSharing**](models.LinkSharing.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListSharesShareDelete**
> ModelsMessage ListsListSharesShareDelete(ctx, list, share)
Remove a link share

Remove a link share. The user needs to have write-access to the list to be able do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **list** | **int32**| List ID | 
  **share** | **int32**| Share Link ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsListSharesShareGet**
> ModelsLinkSharing ListsListSharesShareGet(ctx, list, share)
Get one link shares for a list

Returns one link share by its ID.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **list** | **int32**| List ID | 
  **share** | **int32**| Share ID | 

### Return type

[**ModelsLinkSharing**](models.LinkSharing.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdTeamsGet**
> []ModelsTeamWithRight NamespacesIdTeamsGet(ctx, id, optional)
Get teams on a namespace

Returns a namespace with all teams which have access on a given namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| Namespace ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search teams by its name. | 

### Return type

[**[]ModelsTeamWithRight**](models.TeamWithRight.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdTeamsPut**
> ModelsTeamNamespace NamespacesIdTeamsPut(ctx, id, namespace)
Add a team to a namespace

Gives a team access to a namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 
  **namespace** | [**ModelsTeamNamespace**](ModelsTeamNamespace.md)| The team you want to add to the namespace. | 

### Return type

[**ModelsTeamNamespace**](models.TeamNamespace.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdUsersGet**
> []ModelsUserWithRight NamespacesIdUsersGet(ctx, id, optional)
Get users on a namespace

Returns a namespace with all users which have access on a given namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| Namespace ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search users by its name. | 

### Return type

[**[]ModelsUserWithRight**](models.UserWithRight.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdUsersPut**
> ModelsNamespaceUser NamespacesIdUsersPut(ctx, id, namespace)
Add a user to a namespace

Gives a user access to a namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 
  **namespace** | [**ModelsNamespaceUser**](ModelsNamespaceUser.md)| The user you want to add to the namespace. | 

### Return type

[**ModelsNamespaceUser**](models.NamespaceUser.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesNamespaceIDTeamsTeamIDDelete**
> ModelsMessage NamespacesNamespaceIDTeamsTeamIDDelete(ctx, namespaceID, teamID)
Delete a team from a namespace

Delets a team from a namespace. The team won't have access to the namespace anymore.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespaceID** | **int32**| Namespace ID | 
  **teamID** | **int32**| team ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesNamespaceIDTeamsTeamIDPost**
> ModelsTeamNamespace NamespacesNamespaceIDTeamsTeamIDPost(ctx, namespaceID, teamID, namespace)
Update a team <-> namespace relation

Update a team <-> namespace relation. Mostly used to update the right that team has.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespaceID** | **int32**| Namespace ID | 
  **teamID** | **int32**| Team ID | 
  **namespace** | [**ModelsTeamNamespace**](ModelsTeamNamespace.md)| The team you want to update. | 

### Return type

[**ModelsTeamNamespace**](models.TeamNamespace.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesNamespaceIDUsersUserIDDelete**
> ModelsMessage NamespacesNamespaceIDUsersUserIDDelete(ctx, namespaceID, userID)
Delete a user from a namespace

Delets a user from a namespace. The user won't have access to the namespace anymore.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespaceID** | **int32**| Namespace ID | 
  **userID** | **int32**| user ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesNamespaceIDUsersUserIDPost**
> ModelsNamespaceUser NamespacesNamespaceIDUsersUserIDPost(ctx, namespaceID, userID, namespace)
Update a user <-> namespace relation

Update a user <-> namespace relation. Mostly used to update the right that user has.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespaceID** | **int32**| Namespace ID | 
  **userID** | **int32**| User ID | 
  **namespace** | [**ModelsNamespaceUser**](ModelsNamespaceUser.md)| The user you want to update. | 

### Return type

[**ModelsNamespaceUser**](models.NamespaceUser.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SharesShareAuthPost**
> V1Token SharesShareAuthPost(ctx, share)
Get an auth token for a share

Get a jwt auth token for a shared list from a share hash.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **share** | **string**| The share hash | 

### Return type

[**V1Token**](v1.Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

