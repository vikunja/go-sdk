# ModelsLabel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this label was created. You cannot change this value. | [optional] [default to null]
**CreatedBy** | [***ModelsUser**](models.User.md) | The user who created this label | [optional] [default to null]
**Description** | **string** | The label description. | [optional] [default to null]
**HexColor** | **string** | The color this label has | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this label. | [optional] [default to null]
**Title** | **string** | The title of the lable. You&#39;ll see this one on tasks associated with it. | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this label was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


