# ModelsBulkTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Assignees** | [**[]ModelsUser**](models.User.md) | An array of users who are assigned to this task | [optional] [default to null]
**Attachments** | [**[]ModelsTaskAttachment**](models.TaskAttachment.md) | All attachments this task has | [optional] [default to null]
**Created** | **int32** | A unix timestamp when this task was created. You cannot change this value. | [optional] [default to null]
**CreatedBy** | [***ModelsUser**](models.User.md) | The user who initially created the task. | [optional] [default to null]
**Description** | **string** | The task description. | [optional] [default to null]
**Done** | **bool** | Whether a task is done or not. | [optional] [default to null]
**DoneAt** | **int32** | The unix timestamp when a task was marked as done. | [optional] [default to null]
**DueDate** | **int32** | A unix timestamp when the task is due. | [optional] [default to null]
**EndDate** | **int32** | When this task ends. | [optional] [default to null]
**HexColor** | **string** | The task color in hex | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this task. | [optional] [default to null]
**Labels** | [**[]ModelsLabel**](models.Label.md) | An array of labels which are associated with this task. | [optional] [default to null]
**ListID** | **int32** | The list this task belongs to. | [optional] [default to null]
**PercentDone** | **float32** | Determines how far a task is left from being done | [optional] [default to null]
**Priority** | **int32** | The task priority. Can be anything you want, it is possible to sort by this later. | [optional] [default to null]
**RelatedTasks** | [***ModelsRelatedTaskMap**](models.RelatedTaskMap.md) | All related tasks, grouped by their relation kind | [optional] [default to null]
**ReminderDates** | **[]int32** | An array of unix timestamps when the user wants to be reminded of the task. | [optional] [default to null]
**RepeatAfter** | **int32** | An amount in seconds this task repeats itself. If this is set, when marking the task as done, it will mark itself as \&quot;undone\&quot; and then increase all remindes and the due date by its amount. | [optional] [default to null]
**StartDate** | **int32** | When this task starts. | [optional] [default to null]
**TaskIds** | **[]int32** | A list of task ids to update | [optional] [default to null]
**Text** | **string** | The task text. This is what you&#39;ll see in the list. | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this task was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


