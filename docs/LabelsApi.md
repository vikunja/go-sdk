# \LabelsApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**LabelsGet**](LabelsApi.md#LabelsGet) | **Get** /labels | Get all labels a user has access to
[**LabelsIdDelete**](LabelsApi.md#LabelsIdDelete) | **Delete** /labels/{id} | Delete a label
[**LabelsIdGet**](LabelsApi.md#LabelsIdGet) | **Get** /labels/{id} | Gets one label
[**LabelsIdPut**](LabelsApi.md#LabelsIdPut) | **Put** /labels/{id} | Update a label
[**LabelsPut**](LabelsApi.md#LabelsPut) | **Put** /labels | Create a label
[**TasksTaskIDLabelsBulkPost**](LabelsApi.md#TasksTaskIDLabelsBulkPost) | **Post** /tasks/{taskID}/labels/bulk | Update all labels on a task.
[**TasksTaskLabelsGet**](LabelsApi.md#TasksTaskLabelsGet) | **Get** /tasks/{task}/labels | Get all labels on a task
[**TasksTaskLabelsLabelDelete**](LabelsApi.md#TasksTaskLabelsLabelDelete) | **Delete** /tasks/{task}/labels/{label} | Remove a label from a task
[**TasksTaskLabelsPut**](LabelsApi.md#TasksTaskLabelsPut) | **Put** /tasks/{task}/labels | Add a label to a task


# **LabelsGet**
> []ModelsLabel LabelsGet(ctx, optional)
Get all labels a user has access to

Returns all labels which are either created by the user or associated with a task the user has at least read-access to.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search labels by label text. | 

### Return type

[**[]ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LabelsIdDelete**
> ModelsLabel LabelsIdDelete(ctx, id)
Delete a label

Delete an existing label. The user needs to be the creator of the label to be able to do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Label ID | 

### Return type

[**ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LabelsIdGet**
> ModelsLabel LabelsIdGet(ctx, id)
Gets one label

Returns one label by its ID.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Label ID | 

### Return type

[**ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LabelsIdPut**
> ModelsLabel LabelsIdPut(ctx, id, label)
Update a label

Update an existing label. The user needs to be the creator of the label to be able to do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Label ID | 
  **label** | [**ModelsLabel**](ModelsLabel.md)| The label object | 

### Return type

[**ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LabelsPut**
> ModelsLabel LabelsPut(ctx, label)
Create a label

Creates a new label.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **label** | [**ModelsLabel**](ModelsLabel.md)| The label object | 

### Return type

[**ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDLabelsBulkPost**
> ModelsLabelTaskBulk TasksTaskIDLabelsBulkPost(ctx, label, taskID)
Update all labels on a task.

Updates all labels on a task. Every label which is not passed but exists on the task will be deleted. Every label which does not exist on the task will be added. All labels which are passed and already exist on the task won't be touched.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **label** | [**ModelsLabelTaskBulk**](ModelsLabelTaskBulk.md)| The array of labels | 
  **taskID** | **int32**| Task ID | 

### Return type

[**ModelsLabelTaskBulk**](models.LabelTaskBulk.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskLabelsGet**
> []ModelsLabel TasksTaskLabelsGet(ctx, task, optional)
Get all labels on a task

Returns all labels which are assicociated with a given task.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **task** | **int32**| Task ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **task** | **int32**| Task ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search labels by label text. | 

### Return type

[**[]ModelsLabel**](models.Label.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskLabelsLabelDelete**
> ModelsMessage TasksTaskLabelsLabelDelete(ctx, task, label)
Remove a label from a task

Remove a label from a task. The user needs to have write-access to the list to be able do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **task** | **int32**| Task ID | 
  **label** | **int32**| Label ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskLabelsPut**
> ModelsLabelTask TasksTaskLabelsPut(ctx, task, label)
Add a label to a task

Add a label to a task. The user needs to have write-access to the list to be able do this.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **task** | **int32**| Task ID | 
  **label** | [**ModelsLabelTask**](ModelsLabelTask.md)| The label object | 

### Return type

[**ModelsLabelTask**](models.LabelTask.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

