# ModelsTeamMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Admin** | **bool** | Whether or not the member is an admin of the team. See the docs for more about what a team admin can do | [optional] [default to null]
**Created** | **int32** | A unix timestamp when this relation was created. You cannot change this value. | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this team member relation. | [optional] [default to null]
**Username** | **string** | The username of the member. We use this to prevent automated user id entering. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


