# ModelsListUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this relation was created. You cannot change this value. | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this list &lt;-&gt; user relation. | [optional] [default to null]
**Right** | **int32** | The right this user has. 0 &#x3D; Read only, 1 &#x3D; Read &amp; Write, 2 &#x3D; Admin. See the docs for more details. | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this relation was last updated. You cannot change this value. | [optional] [default to null]
**UserID** | **string** | The username. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


