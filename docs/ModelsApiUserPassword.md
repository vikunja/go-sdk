# ModelsApiUserPassword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** | The user&#39;s email address | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this user. | [optional] [default to null]
**Password** | **string** | The user&#39;s password in clear text. Only used when registering the user. | [optional] [default to null]
**Username** | **string** | The username of the username. Is always unique. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


