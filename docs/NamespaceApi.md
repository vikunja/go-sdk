# \NamespaceApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**NamespaceIdPost**](NamespaceApi.md#NamespaceIdPost) | **Post** /namespace/{id} | Updates a namespace
[**NamespacesGet**](NamespaceApi.md#NamespacesGet) | **Get** /namespaces | Get all namespaces a user has access to
[**NamespacesIdDelete**](NamespaceApi.md#NamespacesIdDelete) | **Delete** /namespaces/{id} | Deletes a namespace
[**NamespacesIdGet**](NamespaceApi.md#NamespacesIdGet) | **Get** /namespaces/{id} | Gets one namespace
[**NamespacesIdListsGet**](NamespaceApi.md#NamespacesIdListsGet) | **Get** /namespaces/{id}/lists | Get all lists in a namespace
[**NamespacesPut**](NamespaceApi.md#NamespacesPut) | **Put** /namespaces | Creates a new namespace


# **NamespaceIdPost**
> ModelsNamespace NamespaceIdPost(ctx, id, namespace)
Updates a namespace

Updates a namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 
  **namespace** | [**ModelsNamespace**](ModelsNamespace.md)| The namespace with updated values you want to update. | 

### Return type

[**ModelsNamespace**](models.Namespace.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesGet**
> []ModelsNamespaceWithLists NamespacesGet(ctx, optional)
Get all namespaces a user has access to

Returns all namespaces a user has access to.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search namespaces by name. | 

### Return type

[**[]ModelsNamespaceWithLists**](models.NamespaceWithLists.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdDelete**
> ModelsMessage NamespacesIdDelete(ctx, id)
Deletes a namespace

Delets a namespace

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdGet**
> ModelsNamespace NamespacesIdGet(ctx, id)
Gets one namespace

Returns a namespace by its ID.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 

### Return type

[**ModelsNamespace**](models.Namespace.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesIdListsGet**
> []ModelsList NamespacesIdListsGet(ctx, id)
Get all lists in a namespace

Returns all lists inside of a namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Namespace ID | 

### Return type

[**[]ModelsList**](models.List.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesPut**
> ModelsNamespace NamespacesPut(ctx, namespace)
Creates a new namespace

Creates a new namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespace** | [**ModelsNamespace**](ModelsNamespace.md)| The namespace you want to create. | 

### Return type

[**ModelsNamespace**](models.Namespace.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

