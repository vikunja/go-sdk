# ModelsLinkSharing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this list was shared. You cannot change this value. | [optional] [default to null]
**Hash** | **string** | The public id to get this shared list | [optional] [default to null]
**Id** | **int32** | The ID of the shared thing | [optional] [default to null]
**Right** | **int32** | The right this list is shared with. 0 &#x3D; Read only, 1 &#x3D; Read &amp; Write, 2 &#x3D; Admin. See the docs for more details. | [optional] [default to null]
**SharedBy** | [***ModelsUser**](models.User.md) | The user who shared this list | [optional] [default to null]
**SharingType** | **int32** | The kind of this link. 0 &#x3D; undefined, 1 &#x3D; without password, 2 &#x3D; with password (currently not implemented). | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this share was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


