# ModelsPasswordReset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewPassword** | **string** | The new password for this user. | [optional] [default to null]
**Token** | **string** | The previously issued reset token. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


