# ModelsTaskRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this label was created. You cannot change this value. | [optional] [default to null]
**CreatedBy** | [***ModelsUser**](models.User.md) | The user who created this relation | [optional] [default to null]
**OtherTaskId** | **int32** | The ID of the other task, the task which is being related. | [optional] [default to null]
**RelationKind** | **string** | The kind of the relation. | [optional] [default to null]
**TaskId** | **int32** | The ID of the \&quot;base\&quot; task, the task which has a relation to another. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


