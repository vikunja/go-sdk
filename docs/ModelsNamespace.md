# ModelsNamespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this namespace was created. You cannot change this value. | [optional] [default to null]
**Description** | **string** | The description of the namespace | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this namespace. | [optional] [default to null]
**Name** | **string** | The name of this namespace. | [optional] [default to null]
**Owner** | [***ModelsUser**](models.User.md) | The user who owns this namespace | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this namespace was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


