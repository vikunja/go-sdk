# ModelsLabelTask

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this task was created. You cannot change this value. | [optional] [default to null]
**LabelId** | **int32** | The label id you want to associate with a task. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


