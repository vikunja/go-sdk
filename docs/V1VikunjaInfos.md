# V1VikunjaInfos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FrontendUrl** | **string** |  | [optional] [default to null]
**LinkSharingEnabled** | **bool** |  | [optional] [default to null]
**MaxFileSize** | **int32** |  | [optional] [default to null]
**Motd** | **string** |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


