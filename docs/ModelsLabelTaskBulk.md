# ModelsLabelTaskBulk

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Labels** | [**[]ModelsLabel**](models.Label.md) | All labels you want to update at once. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


