# ModelsTaskAttachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** |  | [optional] [default to null]
**CreatedBy** | [***ModelsUser**](models.User.md) |  | [optional] [default to null]
**File** | [***FilesFile**](files.File.md) |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**TaskId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


