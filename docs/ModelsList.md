# ModelsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **int32** | A unix timestamp when this list was created. You cannot change this value. | [optional] [default to null]
**Description** | **string** | The description of the list. | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this list. | [optional] [default to null]
**Owner** | [***ModelsUser**](models.User.md) | The user who created this list. | [optional] [default to null]
**Tasks** | [**[]ModelsTask**](models.Task.md) | An array of tasks which belong to the list. | [optional] [default to null]
**Title** | **string** | The title of the list. You&#39;ll see this in the namespace overview. | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this list was last updated. You cannot change this value. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


