# ModelsUserLogin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Password** | **string** | The password for the user. | [optional] [default to null]
**Username** | **string** | The username used to log in. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


