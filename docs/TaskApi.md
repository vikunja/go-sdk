# \TaskApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListsIdPut**](TaskApi.md#ListsIdPut) | **Put** /lists/{id} | Create a task
[**TasksAllGet**](TaskApi.md#TasksAllGet) | **Get** /tasks/all | Get tasks
[**TasksBulkPost**](TaskApi.md#TasksBulkPost) | **Post** /tasks/bulk | Update a bunch of tasks at once
[**TasksIdAttachmentsAttachmentIDDelete**](TaskApi.md#TasksIdAttachmentsAttachmentIDDelete) | **Delete** /tasks/{id}/attachments/{attachmentID} | Delete an attachment
[**TasksIdAttachmentsAttachmentIDGet**](TaskApi.md#TasksIdAttachmentsAttachmentIDGet) | **Get** /tasks/{id}/attachments/{attachmentID} | Get one attachment.
[**TasksIdAttachmentsGet**](TaskApi.md#TasksIdAttachmentsGet) | **Get** /tasks/{id}/attachments | Get  all attachments for one task.
[**TasksIdAttachmentsPut**](TaskApi.md#TasksIdAttachmentsPut) | **Put** /tasks/{id}/attachments | Upload a task attachment
[**TasksIdDelete**](TaskApi.md#TasksIdDelete) | **Delete** /tasks/{id} | Delete a task
[**TasksIdPost**](TaskApi.md#TasksIdPost) | **Post** /tasks/{id} | Update a task
[**TasksTaskIDRelationsDelete**](TaskApi.md#TasksTaskIDRelationsDelete) | **Delete** /tasks/{taskID}/relations | Remove a task relation
[**TasksTaskIDRelationsPut**](TaskApi.md#TasksTaskIDRelationsPut) | **Put** /tasks/{taskID}/relations | Create a new relation between two tasks


# **ListsIdPut**
> ModelsTask ListsIdPut(ctx, id, task)
Create a task

Inserts a task into a list.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
  **task** | [**ModelsTask**](ModelsTask.md)| The task object | 

### Return type

[**ModelsTask**](models.Task.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksAllGet**
> []ModelsTask TasksAllGet(ctx, optional)
Get tasks

Returns all tasks on any list the user has access to.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search tasks by task text. | 
 **sort** | **string**| The sorting parameter. Possible values to sort by are priority, prioritydesc, priorityasc, duedate, duedatedesc, duedateasc. | 
 **startdate** | **int32**| The start date parameter to filter by. Expects a unix timestamp. If no end date, but a start date is specified, the end date is set to the current time. | 
 **enddate** | **int32**| The end date parameter to filter by. Expects a unix timestamp. If no start date, but an end date is specified, the start date is set to the current time. | 

### Return type

[**[]ModelsTask**](models.Task.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksBulkPost**
> ModelsTask TasksBulkPost(ctx, task)
Update a bunch of tasks at once

Updates a bunch of tasks at once. This includes marking them as done. Note: although you could supply another ID, it will be ignored. Use task_ids instead.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **task** | [**ModelsBulkTask**](ModelsBulkTask.md)| The task object. Looks like a normal task, the only difference is it uses an array of list_ids to update. | 

### Return type

[**ModelsTask**](models.Task.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdAttachmentsAttachmentIDDelete**
> ModelsMessage TasksIdAttachmentsAttachmentIDDelete(ctx, id, attachmentID)
Delete an attachment

Delete an attachment.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 
  **attachmentID** | **int32**| Attachment ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdAttachmentsAttachmentIDGet**
> TasksIdAttachmentsAttachmentIDGet(ctx, id, attachmentID)
Get one attachment.

Get one attachment for download. **Returns json on error.**

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 
  **attachmentID** | **int32**| Attachment ID | 

### Return type

 (empty response body)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdAttachmentsGet**
> []ModelsTaskAttachment TasksIdAttachmentsGet(ctx, id)
Get  all attachments for one task.

Get all task attachments for one task.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 

### Return type

[**[]ModelsTaskAttachment**](models.TaskAttachment.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdAttachmentsPut**
> ModelsMessage TasksIdAttachmentsPut(ctx, id, files)
Upload a task attachment

Upload a task attachment. You can pass multiple files with the files form param.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 
  **files** | **string**| The file, as multipart form file. You can pass multiple. | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdDelete**
> ModelsMessage TasksIdDelete(ctx, id)
Delete a task

Deletes a task from a list. This does not mean \"mark it done\".

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksIdPost**
> ModelsTask TasksIdPost(ctx, id, task)
Update a task

Updates a task. This includes marking it as done. Assignees you pass will be updated, see their individual endpoints for more details on how this is done. To update labels, see the description of the endpoint.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Task ID | 
  **task** | [**ModelsTask**](ModelsTask.md)| The task object | 

### Return type

[**ModelsTask**](models.Task.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDRelationsDelete**
> ModelsMessage TasksTaskIDRelationsDelete(ctx, relation, taskID)
Remove a task relation

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **relation** | [**ModelsTaskRelation**](ModelsTaskRelation.md)| The relation object | 
  **taskID** | **int32**| Task ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDRelationsPut**
> ModelsTaskRelation TasksTaskIDRelationsPut(ctx, relation, taskID)
Create a new relation between two tasks

Creates a new relation between two tasks. The user needs to have update rights on the base task and at least read rights on the other task. Both tasks do not need to be on the same list. Take a look at the docs for available task relation kinds.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **relation** | [**ModelsTaskRelation**](ModelsTaskRelation.md)| The relation object | 
  **taskID** | **int32**| Task ID | 

### Return type

[**ModelsTaskRelation**](models.TaskRelation.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

