# \UserApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**LoginPost**](UserApi.md#LoginPost) | **Post** /login | Login
[**RegisterPost**](UserApi.md#RegisterPost) | **Post** /register | Register
[**UserConfirmPost**](UserApi.md#UserConfirmPost) | **Post** /user/confirm | Confirm the email of a new user
[**UserGet**](UserApi.md#UserGet) | **Get** /user | Get user information
[**UserPasswordPost**](UserApi.md#UserPasswordPost) | **Post** /user/password | Change password
[**UserPasswordResetPost**](UserApi.md#UserPasswordResetPost) | **Post** /user/password/reset | Resets a password
[**UserPasswordTokenPost**](UserApi.md#UserPasswordTokenPost) | **Post** /user/password/token | Request password reset token
[**UsersGet**](UserApi.md#UsersGet) | **Get** /users | Get users


# **LoginPost**
> V1Token LoginPost(ctx, credentials)
Login

Logs a user in. Returns a JWT-Token to authenticate further requests.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **credentials** | [**ModelsUserLogin**](ModelsUserLogin.md)| The login credentials | 

### Return type

[**V1Token**](v1.Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RegisterPost**
> ModelsUser RegisterPost(ctx, credentials)
Register

Creates a new user account.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **credentials** | [**ModelsApiUserPassword**](ModelsApiUserPassword.md)| The user credentials | 

### Return type

[**ModelsUser**](models.User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserConfirmPost**
> ModelsMessage UserConfirmPost(ctx, credentials)
Confirm the email of a new user

Confirms the email of a newly registered user.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **credentials** | [**ModelsEmailConfirm**](ModelsEmailConfirm.md)| The token. | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserGet**
> ModelsUser UserGet(ctx, )
Get user information

Returns the current user object.

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**ModelsUser**](models.User.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserPasswordPost**
> ModelsMessage UserPasswordPost(ctx, userPassword)
Change password

Lets the current user change its password.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **userPassword** | [**V1UserPassword**](V1UserPassword.md)| The current and new password. | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserPasswordResetPost**
> ModelsMessage UserPasswordResetPost(ctx, credentials)
Resets a password

Resets a user email with a previously reset token.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **credentials** | [**ModelsPasswordReset**](ModelsPasswordReset.md)| The token with the new password. | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UserPasswordTokenPost**
> ModelsMessage UserPasswordTokenPost(ctx, credentials)
Request password reset token

Requests a token to reset a users password. The token is sent via email.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **credentials** | [**ModelsPasswordTokenRequest**](ModelsPasswordTokenRequest.md)| The username of the user to request a token for. | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UsersGet**
> []ModelsUser UsersGet(ctx, optional)
Get users

Lists all users (without emailadresses). Also possible to search for a specific user.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **s** | **string**| Search for a user by its name. | 

### Return type

[**[]ModelsUser**](models.User.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

