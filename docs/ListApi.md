# \ListApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ListsGet**](ListApi.md#ListsGet) | **Get** /lists | Get all lists a user has access to
[**ListsIdDelete**](ListApi.md#ListsIdDelete) | **Delete** /lists/{id} | Deletes a list
[**ListsIdGet**](ListApi.md#ListsIdGet) | **Get** /lists/{id} | Gets one list
[**ListsIdListusersGet**](ListApi.md#ListsIdListusersGet) | **Get** /lists/{id}/listusers | Get users
[**ListsIdPost**](ListApi.md#ListsIdPost) | **Post** /lists/{id} | Updates a list
[**NamespacesNamespaceIDListsPut**](ListApi.md#NamespacesNamespaceIDListsPut) | **Put** /namespaces/{namespaceID}/lists | Creates a new list


# **ListsGet**
> []ModelsList ListsGet(ctx, optional)
Get all lists a user has access to

Returns all lists a user has access to.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search lists by title. | 

### Return type

[**[]ModelsList**](models.List.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdDelete**
> ModelsMessage ListsIdDelete(ctx, id)
Deletes a list

Delets a list

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdGet**
> ModelsList ListsIdGet(ctx, id)
Gets one list

Returns a list by its ID.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 

### Return type

[**ModelsList**](models.List.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdListusersGet**
> []ModelsUser ListsIdListusersGet(ctx, id, optional)
Get users

Lists all users (without emailadresses). Also possible to search for a specific user.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| List ID | 
 **s** | **string**| Search for a user by its name. | 

### Return type

[**[]ModelsUser**](models.User.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ListsIdPost**
> ModelsList ListsIdPost(ctx, id, list)
Updates a list

Updates a list. This does not include adding a task (see below).

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| List ID | 
  **list** | [**ModelsList**](ModelsList.md)| The list with updated values you want to update. | 

### Return type

[**ModelsList**](models.List.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NamespacesNamespaceIDListsPut**
> ModelsList NamespacesNamespaceIDListsPut(ctx, namespaceID, list)
Creates a new list

Creates a new list in a given namespace. The user needs write-access to the namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **namespaceID** | **int32**| Namespace ID | 
  **list** | [**ModelsList**](ModelsList.md)| The list you want to create. | 

### Return type

[**ModelsList**](models.List.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

