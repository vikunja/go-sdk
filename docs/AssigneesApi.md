# \AssigneesApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TasksTaskIDAssigneesBulkPost**](AssigneesApi.md#TasksTaskIDAssigneesBulkPost) | **Post** /tasks/{taskID}/assignees/bulk | Add multiple new assignees to a task
[**TasksTaskIDAssigneesGet**](AssigneesApi.md#TasksTaskIDAssigneesGet) | **Get** /tasks/{taskID}/assignees | Get all assignees for a task
[**TasksTaskIDAssigneesPut**](AssigneesApi.md#TasksTaskIDAssigneesPut) | **Put** /tasks/{taskID}/assignees | Add a new assignee to a task
[**TasksTaskIDAssigneesUserIDDelete**](AssigneesApi.md#TasksTaskIDAssigneesUserIDDelete) | **Delete** /tasks/{taskID}/assignees/{userID} | Delete an assignee


# **TasksTaskIDAssigneesBulkPost**
> ModelsTaskAssginee TasksTaskIDAssigneesBulkPost(ctx, assignee, taskID)
Add multiple new assignees to a task

Adds multiple new assignees to a task. The assignee needs to have access to the list, the doer must be able to edit this task. Every user not in the list will be unassigned from the task, pass an empty array to unassign everyone.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **assignee** | [**ModelsBulkAssignees**](ModelsBulkAssignees.md)| The array of assignees | 
  **taskID** | **int32**| Task ID | 

### Return type

[**ModelsTaskAssginee**](models.TaskAssginee.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDAssigneesGet**
> []ModelsUser TasksTaskIDAssigneesGet(ctx, taskID, optional)
Get all assignees for a task

Returns an array with all assignees for this task.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **taskID** | **int32**| Task ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskID** | **int32**| Task ID | 
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search assignees by their username. | 

### Return type

[**[]ModelsUser**](models.User.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDAssigneesPut**
> ModelsTaskAssginee TasksTaskIDAssigneesPut(ctx, assignee, taskID)
Add a new assignee to a task

Adds a new assignee to a task. The assignee needs to have access to the list, the doer must be able to edit this task.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **assignee** | [**ModelsTaskAssginee**](ModelsTaskAssginee.md)| The assingee object | 
  **taskID** | **int32**| Task ID | 

### Return type

[**ModelsTaskAssginee**](models.TaskAssginee.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TasksTaskIDAssigneesUserIDDelete**
> ModelsMessage TasksTaskIDAssigneesUserIDDelete(ctx, taskID, userID)
Delete an assignee

Un-assign a user from a task.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **taskID** | **int32**| Task ID | 
  **userID** | **int32**| Assignee user ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

