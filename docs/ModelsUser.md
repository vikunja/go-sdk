# ModelsUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AvatarUrl** | **string** | The users md5-hashed email address, used to get the avatar from gravatar and the likes. | [optional] [default to null]
**Created** | **int32** | A unix timestamp when this task was created. You cannot change this value. | [optional] [default to null]
**Email** | **string** | The user&#39;s email address. | [optional] [default to null]
**Id** | **int32** | The unique, numeric id of this user. | [optional] [default to null]
**Updated** | **int32** | A unix timestamp when this task was last updated. You cannot change this value. | [optional] [default to null]
**Username** | **string** | The username of the user. Is always unique. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


