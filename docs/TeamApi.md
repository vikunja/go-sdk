# \TeamApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TeamsGet**](TeamApi.md#TeamsGet) | **Get** /teams | Get teams
[**TeamsIdDelete**](TeamApi.md#TeamsIdDelete) | **Delete** /teams/{id} | Deletes a team
[**TeamsIdMembersPut**](TeamApi.md#TeamsIdMembersPut) | **Put** /teams/{id}/members | Add a user to a team
[**TeamsIdMembersUserIDDelete**](TeamApi.md#TeamsIdMembersUserIDDelete) | **Delete** /teams/{id}/members/{userID} | Remove a user from a team
[**TeamsIdPost**](TeamApi.md#TeamsIdPost) | **Post** /teams/{id} | Updates a team
[**TeamsPut**](TeamApi.md#TeamsPut) | **Put** /teams | Creates a new team


# **TeamsGet**
> []ModelsTeam TeamsGet(ctx, optional)
Get teams

Returns all teams the current user is part of.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **p** | **int32**| The page number. Used for pagination. If not provided, the first page of results is returned. | 
 **s** | **string**| Search teams by its name. | 

### Return type

[**[]ModelsTeam**](models.Team.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TeamsIdDelete**
> ModelsMessage TeamsIdDelete(ctx, id)
Deletes a team

Delets a team. This will also remove the access for all users in that team.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Team ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TeamsIdMembersPut**
> ModelsTeamMember TeamsIdMembersPut(ctx, id, team)
Add a user to a team

Add a user to a team.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Team ID | 
  **team** | [**ModelsTeamMember**](ModelsTeamMember.md)| The user to be added to a team. | 

### Return type

[**ModelsTeamMember**](models.TeamMember.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TeamsIdMembersUserIDDelete**
> ModelsMessage TeamsIdMembersUserIDDelete(ctx, id, userID)
Remove a user from a team

Remove a user from a team. This will also revoke any access this user might have via that team.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Team ID | 
  **userID** | **int32**| User ID | 

### Return type

[**ModelsMessage**](models.Message.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TeamsIdPost**
> ModelsTeam TeamsIdPost(ctx, id, team)
Updates a team

Updates a team.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **id** | **int32**| Team ID | 
  **team** | [**ModelsTeam**](ModelsTeam.md)| The team with updated values you want to update. | 

### Return type

[**ModelsTeam**](models.Team.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TeamsPut**
> ModelsTeam TeamsPut(ctx, team)
Creates a new team

Creates a new team in a given namespace. The user needs write-access to the namespace.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **team** | [**ModelsTeam**](ModelsTeam.md)| The team you want to create. | 

### Return type

[**ModelsTeam**](models.Team.md)

### Authorization

[JWTKeyAuth](../README.md#JWTKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

