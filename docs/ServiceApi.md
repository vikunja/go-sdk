# \ServiceApi

All URIs are relative to *https://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**InfoGet**](ServiceApi.md#InfoGet) | **Get** /info | Info


# **InfoGet**
> V1VikunjaInfos InfoGet(ctx, )
Info

Returns the version, frontendurl, motd and various settings of Vikunja

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**V1VikunjaInfos**](v1.vikunjaInfos.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

